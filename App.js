import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View, Image,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { NavigationContainer } from '@react-navigation/native';
// import MyStack from "./src/route/index";
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Button from './src/components/Button';
import HeaderComponent from './src/components/HeaderComponent';
import Login from './src/screen/Login'
import AddSecurityGuard from './src/screen/AddSecurityGuard'
import ManageComplaints from './src/screen/ManageComplaintsScreen';
import DropDownComponent from './src/components/DropdownComponent'
import CloseComplaints from './src/screen/CloseComplaints'
import MyComplaintsCloseScreen from './src/screen/MyComplaintsCloseScreen'
import AddEnquriEmptyScreen from "./src/screen/AddEnquriEmpty";
import UtilitySchedule from './src/screen/UtilityScheduleScreen'
import AddExpensescreen from './src/screen/AddExpenseScreen'
import ConfirmEnquiry from './src/screen/ConfirmEnquiry'
import AddExpense from './src/screen/AddExpense'
import AddFlats from './src/screen/AddFlatsScreen'
import FlatsLists from './src/screen/FlatsListScreen'
import ProjectList from './src/screen/ProjectListScreen'
import ExpenseList from './src/screen/ExpenseListScreen'
import ProjectDetails from './src/screen/ProjectDetails'
import AddWings from './src/screen/AddWings'
import ManageDocuments from './src/screen/ManageDocumentScreen'
import SplashScreen from './src/screen/SplashScreen'
import CurrentProject from './src/screen/CurrentProject'
import MangeCustomer from './src/screen/ManageCustomer'
import ManageExpense from './src/screen/ManageExpenseScreen'
import AddCustomer from './src/screen/AddCustomerScreen'
import ManageInvoice from './src/screen/ManageInvoice'
import UtilitiesScreen from './src/screen/Utilities'
import UtilitiesDownStatus from './src/screen/UtilitiesDownStatus'
import AmenityBooking from './src/screen/AmenityBooking'
import ManageAmenities from './src/screen/ManageAmenitiesScreen'
import VendorTypes from './src/screen/VendorTypes'
import AddVendorScreen from './src/screen/AddVendorScreen'
import RateVendorScreen from './src/screen/RateVendorScreen'
import VendorList from './src/screen/VendorList'
import VendorRating from './src/screen/VendorRating'
import AddComplaints from './src/screen/AddComplaints'
import ManageSecurityGuard from './src/screen/ManageSecurityGuard'
import AddGuestVehicle from './src/screen/AddGuestVehicle'
import ManageEnquiry from './src/screen/ManageEnqury'
import AddProject from './src/screen/AddProject'
import DashboardScreen from './src/screen/DashboardScreen'
import { heightPercentageToDP } from 'react-native-responsive-screen';
import DrawerContent from './src/route/DrawerContent'
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

function introStack() {
  return (
    <Stack.Navigator initialRouteName={'SplashScreen'} headerMode="none" swipeEnabled={false} gesturesEnabled={false}>
      <Stack.Screen name="SplashScreen" component={SplashScreen} swipeEnabled={false} gesturesEnabled={false} />
      <Stack.Screen name="Login" component={Login} swipeEnabled={false} gesturesEnabled={false} />
    </Stack.Navigator>
  )
}

function MyStack() {
  return (
    <Stack.Navigator initialRouteName={'introStack'} headerMode="none">
      <Stack.Screen name="introStack" component={introStack} />
      <Stack.Screen name="Button" component={Button} />
      <Stack.Screen name="HeaderComponent" component={HeaderComponent} />
      {/* <Stack.Screen name="Login" component={Login} /> */}
      <Stack.Screen name="AddSecurityGuard" component={AddSecurityGuard} />
      <Stack.Screen name="ManageComplaints" component={ManageComplaints} />
      <Stack.Screen name="DropDownComponent" component={DropDownComponent} />
      <Stack.Screen name="CloseComplaints" component={CloseComplaints} />
      <Stack.Screen name="MyComplaintsCloseScreen" component={MyComplaintsCloseScreen} />
      <Stack.Screen name="AddEnquriEmptyScreen" component={AddEnquriEmptyScreen} />
      <Stack.Screen name="UtilitySchedule" component={UtilitySchedule} />
      <Stack.Screen name="AddExpensescreen" component={AddExpensescreen} />
      <Stack.Screen name="ConfirmEnquiry" component={ConfirmEnquiry} />
      <Stack.Screen name="AddExpense" component={AddExpense} />
      <Stack.Screen name="AddFlats" component={AddFlats} />
      <Stack.Screen name="FlatsLists" component={FlatsLists} />
      <Stack.Screen name="ProjectList" component={ProjectList} />
      <Stack.Screen name="ExpenseList" component={ExpenseList} />
      <Stack.Screen name="ProjectDetails" component={ProjectDetails} />
      <Stack.Screen name="AddWings" component={AddWings} />
      <Stack.Screen name="ManageDocuments" component={ManageDocuments} />
      {/* <Stack.Screen name="SplashScreen" component={SplashScreen} /> */}
      <Stack.Screen name="AddCustomer" component={AddCustomer} />
      <Stack.Screen name="CurrentProject" component={CurrentProject} />
      <Stack.Screen name="MangeCustomer" component={MangeCustomer} />
      <Stack.Screen name="ManageExpense" component={ManageExpense} />
      <Stack.Screen name="ManageInvoice" component={ManageInvoice} />
      <Stack.Screen name="UtilitiesScreen" component={UtilitiesScreen} />
      <Stack.Screen name="UtilitiesDownStatus" component={UtilitiesDownStatus} />
      <Stack.Screen name="ManageAmenities" component={ManageAmenities} />
      <Stack.Screen name="AmenityBooking" component={AmenityBooking} />
      <Stack.Screen name="VendorTypes" component={VendorTypes} />
      <Stack.Screen name="AddVendorScreen" component={AddVendorScreen} />
      <Stack.Screen name="VendorList" component={VendorList} />
      <Stack.Screen name="VendorRating" component={VendorRating} />
      <Stack.Screen name="RateVendorScreen" component={RateVendorScreen} />
      <Stack.Screen name="AddComplaints" component={AddComplaints} />
      <Stack.Screen name="ManageSecurityGuard" component={ManageSecurityGuard} />
      <Stack.Screen name="AddGuestVehicle" component={AddGuestVehicle} />
      <Stack.Screen name="ManageEnquiry" component={ManageEnquiry} />
      <Stack.Screen name="AddProject" component={AddProject} />
      <Stack.Screen name="DashboardScreen" component={DashboardScreen} />
    </Stack.Navigator>
  );
}
// function AppNavigations() {
//   return (
// <NavigationContainer>
//   <Drawer.Navigator >
//     <Drawer.Screen name="Home" component={MyStack} />
//     <Drawer.Screen name="AddSecurityGuard" component={AddSecurityGuard} />
//     <Drawer.Screen name="ManageComplaints" component={ManageComplaints} />
//     <Drawer.Screen name="CloseComplaints" component={CloseComplaints} />
//     <Drawer.Screen name="MyComplaintsCloseScreen" component={MyComplaintsCloseScreen} />
//     <Drawer.Screen name="AddEnquriEmptyScreen" component={AddEnquriEmptyScreen} />
//     <Drawer.Screen name="UtilitySchedule" component={UtilitySchedule} />
//     {/* <Drawer.Screen name="AddExpensescreen" component={AddExpensescreen} /> */}
//     <Drawer.Screen name="ConfirmEnquiry" component={ConfirmEnquiry} />
//     <Drawer.Screen name="AddExpense" component={AddExpense} />
//     <Drawer.Screen name="AddFlats" component={AddFlats} />
//     <Drawer.Screen name="FlatsLists" component={FlatsLists} />
//     <Drawer.Screen name="ProjectList" component={ProjectList} />
//     <Drawer.Screen name="ExpenseList" component={ExpenseList} />
//     <Drawer.Screen name="ProjectDetails" component={ProjectDetails} />
//     <Drawer.Screen name="AddWings" component={AddWings} />
//     <Drawer.Screen name="ManageDocuments" component={ManageDocuments} />
//   </Drawer.Navigator>
// </NavigationContainer>
//   );
// }
// export default AppNavigations;
// const App: () => React$Node = () => {
// export default function App() {
//   return (
//     <>
//       <NavigationContainer>
//         <MyStack />
//       </NavigationContainer>

//     </>
//   );
// };
const DrawerNavigator = (props) => {
  var expand = true
  return (
    // <NavigationContainer>
    //   <Drawer.Navigator >
    //     <Drawer.Screen name="Home" component={MyStack} />
    //     <Drawer.Screen name="CurrentProject" component={CurrentProject}

    //       // options={{ headerStyleInterpolator: forFade ,title:"home" ,
    //       // icon:<Image source={require('./src/assets/CalendarBlue.png')} style={styles.drawerActive}/> }} />
    //       options={{
    //         title: 'Current Projects',
    //         drawerIcon: ({ focused, size }) => (
    //           <Image
    //             source={require('./src/assets/CalendarBlue.png')}
    //             style={[focused ? styles.drawerActive : styles.drawerInActive, { height: size, width: size }]}
    //           />
    //         )
    //       }} />
    //     <Drawer.Screen name="MangeCustomer" component={MangeCustomer}

    //       // options={{ headerStyleInterpolator: forFade ,title:"home" ,
    //       // icon:<Image source={require('./src/assets/CalendarBlue.png')} style={styles.drawerActive}/> }} />
    //       options={{
    //         title: 'Manage Customer',
    //         drawerIcon: ({ focused, size }) => (
    //           <Image
    //             source={require('./src/assets/CalendarBlue.png')}
    //             style={[focused ? styles.drawerActive : styles.drawerInActive, { height: size, width: size }]}
    //           />
    //         )
    //       }} />
    //     <Drawer.Screen name="MangeEnquiry" component={AddCustomer}

    //       // options={{ headerStyleInterpolator: forFade ,title:"home" ,
    //       // icon:<Image source={require('./src/assets/CalendarBlue.png')} style={styles.drawerActive}/> }} />
    //       options={{
    //         title: 'Manage Enquiry',
    //         drawerIcon: ({ focused, size }) => (
    //           <Image
    //             source={require('./src/assets/CalendarBlue.png')}
    //             style={[focused ? styles.drawerActive : styles.drawerInActive, { height: size, width: size }]}
    //           />
    //         )
    //       }} />
    //     {/* <Drawer.Screen name="MangeCustomer" component={MangeCustomer}

    //       // options={{ headerStyleInterpolator: forFade ,title:"home" ,
    //       // icon:<Image source={require('./src/assets/CalendarBlue.png')} style={styles.drawerActive}/> }} />
    //       options={{
    //         title: 'Manage Customer',
    //         drawerIcon: ({ focused, size }) => (
    //           <Image
    //             source={require('./src/assets/CalendarBlue.png')}
    //             style={[focused ? styles.drawerActive : styles.drawerInActive, { height: size, width: size }]}
    //           />
    //         )
    //       }} /> */}
    //     {/* <Drawer.Screen name="AddCustomer" component={AddCustomer}

    //       // options={{ headerStyleInterpolator: forFade ,title:"home" ,
    //       // icon:<Image source={require('./src/assets/CalendarBlue.png')} style={styles.drawerActive}/> }} />
    //       options={{
    //         title: 'Add Customer',
    //         drawerIcon: ({ focused, size }) => (
    //           <Image
    //             source={require('./src/assets/CalendarBlue.png')}
    //             style={[focused ? styles.drawerActive : styles.drawerInActive, { height: size, width: size }]}
    //           />
    //         )
    //       }} />
    //     <Drawer.Screen name="AddSecurityGuard" component={AddSecurityGuard}

    //       // options={{ headerStyleInterpolator: forFade ,title:"home" ,
    //       // icon:<Image source={require('./src/assets/CalendarBlue.png')} style={styles.drawerActive}/> }} />
    //       options={{
    //         title: 'AddSecurity Guard',
    //         drawerIcon: ({ focused, size }) => (
    //           <Image
    //             source={require('./src/assets/CalendarBlue.png')}
    //             style={[focused ? styles.drawerActive : styles.drawerInActive, { height: size, width: size }]}
    //           />
    //         )
    //       }} />

    //     <Drawer.Screen name="ManageComplaints" component={ManageComplaints} />
    //     <Drawer.Screen name="CloseComplaints" component={CloseComplaints} />
    //     <Drawer.Screen name="MyComplaintsCloseScreen" component={MyComplaintsCloseScreen} />
    //     <Drawer.Screen name="AddEnquriEmptyScreen" component={AddEnquriEmptyScreen} />
    //     <Drawer.Screen name="UtilitySchedule" component={UtilitySchedule} />
    //         <Drawer.Screen name="ConfirmEnquiry" component={ConfirmEnquiry} />
    //     <Drawer.Screen name="AddExpense" component={AddExpense} />
    //     <Drawer.Screen name="AddFlats" component={AddFlats} />
    //     <Drawer.Screen name="FlatsLists" component={FlatsLists} />
    //     <Drawer.Screen name="ProjectList" component={ProjectList} />
    //     <Drawer.Screen name="ExpenseList" component={ExpenseList} />
    //     <Drawer.Screen name="ProjectDetails" component={ProjectDetails} />
    //     <Drawer.Screen name="AddWings" component={AddWings} />
    //     <Drawer.Screen name="ManageDocuments" component={ManageDocuments} /> */}

    //   </Drawer.Navigator>
    // </NavigationContainer>
    <NavigationContainer>
      <Drawer.Navigator initialRouteName={'MyStack'} headerMode="none" swipeEnabled={false} gesturesEnabled={false}
        drawerLockMode={'locked-close'}
        // drawerStyle={{
        //   width: heightPercentageToDP("30%"),
        // }}
        // edgeWidth={props.routeName === 'home' ? 40 : 0}
        edgeWidth={0}
        drawerContent={(props) => <DrawerContent {...props} />}>

        <Drawer.Screen name={'App'} component={MyStack}

        // options={{ headerStyleInterpolator: forFade ,title:"home" ,
        // icon:<Image source={require('./src/assets/CalendarBlue.png')} style={styles.drawerActive}/> }} />
        // options={{
        //   title: 'Manage Customer',
        //   drawerIcon: ({ focused, size }) => (
        //     <Image
        //       source={require('./src/assets/CalendarBlue.png')}
        //       style={[focused ? styles.drawerActive : styles.drawerInActive, { height: size, width: size }]}
        //     />
        //   )
        // }}
        />
        {/* <Drawer.Screen name={'App'} component={MyStack} /> */}
      </Drawer.Navigator>

    </NavigationContainer>
  );
};

export default DrawerNavigator;
const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

// export default App;
